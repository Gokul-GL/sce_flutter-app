import 'package:SCE/about_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class AdmissionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebviewScaffold(
        appBar: AppBar(
          title: Text(
            "Gallery",
            textAlign: TextAlign.center,
          ),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
              );
            },
          ),
        ),
        url:
            "https://docs.google.com/forms/d/1E1ca0JkggaYJNwhjuSqTt20RUd0tevFrAGHFq8bwniQ/viewform?edit_requested=true",
        withJavascript: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
    );
  }
}
