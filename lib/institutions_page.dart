import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class InstitutionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              'Sasurie Institutions',
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.5,
              ),
            ),
          ],
        ),
      ),
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0.0, 2.0),
              blurRadius: 6.0,
            ),
          ],
        ),
        child: SizedBox(
            height: 150.0,
            width: 350.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Carousel(
                images: [
                  NetworkImage(
                      'https://image3.mouthshut.com/images/Restaurant/Photo/-85675_157557.jpg'),
                  NetworkImage(
                      'https://3.bp.blogspot.com/-6E3o3GFYg4U/Vxd1d6zmJ3I/AAAAAAAAUow/1zurNywjZFIK1FJehQW2WMi0BTac_pn4ACLcB/s1600/v2712.png'),
                  NetworkImage(
                      'https://www.collegeandstudents.com/wp-content/uploads/2019/09/Sasurie-cas.png'),
                  NetworkImage('http://nyruthi.in/arts/img/NyruthiNewLogo.png'),
                  NetworkImage(
                      "http://nyruthi.in/npc/img/NyruthiPolytechnic_Logo.jpg"),
                ],
                boxFit: BoxFit.fill,
                dotSize: 4.0,
                dotSpacing: 15.0,
                dotColor: Colors.lightGreenAccent,
                indicatorBgPadding: 5.0,
                //dotBgColor: Colors.purple.withOpacity(0.5),
                borderRadius: true,
                moveIndicatorFromBottom: 180.0,
                noRadiusForIndicator: true,
              ),
            )),
      )
    ]);
  }
}
