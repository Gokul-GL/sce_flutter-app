import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class LastPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: [
                Image.asset(
                  "images/sce1.png",
                  height: 100,
                  width: 320,
                  fit: BoxFit.fill,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: [
                Text(
                  'For More Information Visite Our WebSite',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.0,
                  ),
                ),
                FlatButton(
                    onPressed: () {
                      launchURL();
                    },
                    child: Text("http://www.sasurieengg.com")),
                Text(
                  "Developed by : Sasurie Infotech",
                  style: GoogleFonts.oleoScript(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'Developer : Mr.GL',
                  style: GoogleFonts.satisfy(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
      Row(
        children: [
          Image.asset(
            "images/sitlogo.png",
            fit: BoxFit.fill,
            height: 70,
            width: 110,
          ),
        ],
      )
    ]);
  }
}

launchURL() async {
  const url = 'http://www.sasurieengg.com';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
