import 'package:SCE/about_page.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:device_id/device_id.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreenExample(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          DrawerScreen(),
          HomeScreen(),
        ],
      ),
    );
  }
}

class SplashScreenExample extends StatefulWidget {
  const SplashScreenExample() : super();

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreenExample> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final fb = FirebaseDatabase.instance;
  final name = "Name";
  int count = 1;
  double percentage = 0.0;
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 9),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => MyHomePage())));
    Timer.periodic(new Duration(seconds: 2), (timer) {
      if (count == 9) {
        timer.cancel();
      } else {
        percentage = (count * 2) / 10;
        setState(() {});
        count++;
      }
    });
    _token();
  }

  _token() async {
    var deviceid = await DeviceId.getID;
    final ref = fb.reference();
    var token = await _firebaseMessaging.getToken();
    ref.push().child("User").child(deviceid).set(token);
    print("########################Instance ID: " + token);
  }

  // Offset _offset = Offset.zero; // changed
  @override
  Widget build(BuildContext context) {
    // final ref = fb.reference();
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Image.asset(
            "images/splashbg.jpg",
            fit: BoxFit.cover,
          ),
          Center(
              child: Container(
            child: SizedBox(
              //width: 250.0,
              child: FadeAnimatedTextKit(
                onTap: () {
                  print("Tap Event");
                },
                text: [" ", "do IT!", "do IT RIGHT!!", "do IT RIGHT NOW!!!"],

                textStyle: TextStyle(
                    //fontFamily: GoogleFonts(),
                    fontFamily: "Horizon",
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.pink),
                textAlign: TextAlign.start,
                // alignment:
                //     AlignmentDirectional.topStart // or Alignment.topLeft
              ),
            ),
          ))
        ],
      ),
      // body: Container(
      //   decoration: BoxDecoration(
      //     image: DecorationImage(
      //       image: AssetImage("images/splashbg.jpg"),
      //     ),
      //   ),
      //   child: SizedBox(
      //     width: 250.0,
      //     child: FadeAnimatedTextKit(
      //         onTap: () {
      //           print("Tap Event");
      //         },
      //         text: ["do IT!", "do it RIGHT!!", "do it RIGHT NOW!!!"],
      //         textStyle: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
      //         textAlign: TextAlign.start,
      //         alignment: AlignmentDirectional.topStart // or Alignment.topLeft
      //         ),
      //   ),
      // ),
    );
  }
}
