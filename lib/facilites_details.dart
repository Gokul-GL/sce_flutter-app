import 'package:SCE/about_page.dart';
import 'package:flutter/material.dart';

class FacilitesDetailScreen extends StatefulWidget {
  const FacilitesDetailScreen({Key key, this.hotel}) : super(key: key);
  final Hotel hotel;
  //FacilitesDetailScreen({this.hotel});
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<FacilitesDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: AssetImage("images/bg1.jpg"), fit: BoxFit.cover)),
      child: GestureDetector(
        child: Center(
          child: Container(
              height: 400,
              width: 250,
              padding: EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 15.0,
              ),
              decoration: BoxDecoration(
                  // color: Colors.lightBlueAccent,
                  color: Colors.cyanAccent[100],
                  borderRadius: BorderRadius.circular(40.0),
                  border: Border.all(color: Colors.blueGrey, width: 2),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.blueGrey[50],
                      blurRadius: 10.0, // soften the shadow
                      spreadRadius: 3.0, //extend the shadow
                      offset: Offset(
                        6.0, // Move to right 10  horizontally
                        6.0, // Move to bottom 10 Vertically
                      ),
                    ),
                  ]),
              //color: Colors.lightGreenAccent,
              child: ListView(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      // Image.asset("images/bg.jpg"),
                      Hero(
                        tag: widget.hotel.imageUrl,
                        child: Column(
                          children: <Widget>[
                            Image.network(
                              widget.hotel.imageUrl,
                              fit: BoxFit.fill,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                widget.hotel.name,
                                style: TextStyle(
                                    fontSize: 18,
                                    fontStyle: FontStyle.italic,
                                    color: Colors.black),
                              ),
                            ),
                            SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Stack(
                                  children: <Widget>[
                                    Text(
                                      widget.hotel.description,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black),
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              )),
        ),
        onDoubleTap: () {
          Navigator.pop(context);
        },
      ),
    ));
  }
}
