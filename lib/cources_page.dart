import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class CoursesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              'Courses',
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.5,
              ),
            ),
          ],
        ),
      ),
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0.0, 2.0),
              blurRadius: 6.0,
            ),
          ],
        ),
        child: SizedBox(
            height: 150.0,
            width: 350.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Carousel(
                images: [
                  NetworkImage(
                      'https://firebasestorage.googleapis.com/v0/b/fir-example-f8088.appspot.com/o/ugcourse.jpg?alt=media&token=fac43ad4-1e44-4847-83cb-5ff1490c978b'),
                  NetworkImage(
                      'https://firebasestorage.googleapis.com/v0/b/fir-example-f8088.appspot.com/o/pgcourses.jpg?alt=media&token=d0db2bbb-314e-4b8d-9764-37657f3391f1'),
                ],
                boxFit: BoxFit.fill,
                dotSize: 4.0,
                dotSpacing: 15.0,
                dotColor: Colors.lightGreenAccent,
                indicatorBgPadding: 5.0,
                //dotBgColor: Colors.purple.withOpacity(0.5),
                borderRadius: true,
                moveIndicatorFromBottom: 180.0,
                noRadiusForIndicator: true,
              ),
            )),
      )
    ]);
  }
}
