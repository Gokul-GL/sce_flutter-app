import 'package:SCE/about_page.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key key, this.destination}) : super(key: key);
  final Destination destination;

  //DestinationScreen({this.destination});
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: AssetImage("images/bg1.jpg"), fit: BoxFit.cover)),
      child: GestureDetector(
        child: Center(
          child: Container(
            height: 300,
            width: 250,
            padding: EdgeInsets.symmetric(
              horizontal: 20.0,
              vertical: 15.0,
            ),
            decoration: BoxDecoration(
                // color: Colors.lightBlueAccent,
                color: Colors.cyanAccent[100],
                borderRadius: BorderRadius.circular(40.0),
                border: Border.all(color: Colors.blueGrey, width: 2),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blueGrey[50],
                    blurRadius: 10.0, // soften the shadow
                    spreadRadius: 3.0, //extend the shadow
                    offset: Offset(
                      6.0, // Move to right 10  horizontally
                      6.0, // Move to bottom 10 Vertically
                    ),
                  ),
                ]),
            //color: Colors.lightGreenAccent,
            child: Stack(
              children: <Widget>[
                // Image.asset("images/bg.jpg"),
                Hero(
                  tag: widget.destination.imageUrl,
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Image.network(
                          widget.destination.imageUrl,
                          fit: BoxFit.fill,
                          height: 180,
                          width: 140,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Text(
                          widget.destination.city,
                          style: TextStyle(
                              fontSize: 18,
                              fontStyle: FontStyle.italic,
                              color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Text(
                          widget.destination.description,
                          style: TextStyle(
                              fontSize: 16,
                              fontStyle: FontStyle.italic,
                              color: Colors.black),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        onDoubleTap: () {
          Navigator.pop(context);
        },
      ),
    ));
  }
}
