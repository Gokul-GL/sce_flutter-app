<<<<<<< HEAD
import 'package:flutter/cupertino.dart';
=======
import 'package:SCE/admission_page.dart';
import 'package:SCE/cources_page.dart';
import 'package:SCE/gallery_image.dart';
import 'package:SCE/institutions_page.dart';
import 'package:SCE/last_page.dart';
import 'package:SCE/placement_records.dart';
>>>>>>> 31f634a31a537cb963b54935deb2fae6825f6990
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  IconData icon;
  String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: RadialGradient(
        radius: 1,
        colors: [
          const Color(0xff80deea),
          const Color(0xff03a9f4),
        ],
        //center: Alignment(-0.3, -0.2)
      )),
      child: Padding(
        padding:
            const EdgeInsets.only(top: 100, bottom: 100, left: 20, right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  'SwipeUp',
                  style: GoogleFonts.pacifico(
                      color: Colors.white70,
                      fontSize: 28,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'SCE',
                  style: GoogleFonts.pacifico(
                      color: Colors.blue,
                      fontSize: 28,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                buildCategories(Icons.business, 'CSE'),
                SizedBox(
                  height: 20,
                ),
                buildCategories(Icons.live_tv, 'ECE'),
                SizedBox(
                  height: 20,
                ),
                buildCategories(Icons.spa, 'EEE'),
                SizedBox(
                  height: 20,
                ),
                buildCategories(Icons.directions_run, 'Civil'),
                SizedBox(
                  height: 20,
                ),
                buildCategories(Icons.computer, 'Mech'),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
            buildCategories(Icons.settings, 'Sasurie Infotech')
          ],
        ),
      ),
    );
  }

  Row buildCategories(IconData icon, String title) {
    return Row(
      children: <Widget>[
        Icon(
          icon,
          color: const Color(0xfff5f5f5),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          title,
          style: GoogleFonts.ptSans(
              color: const Color(0xfff5f5f5),
              fontSize: 14,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}

//home page

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;
  bool isDrawerOpen = false;

  final controller = PageController(
    initialPage: 1,
  );

  @override
  //home screen widget
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        if (details.delta.dx > 1) {
          // Right Swipe
          setState(() {
            xOffset = 230;
            yOffset = 120;
            scaleFactor = 0.75;
            isDrawerOpen = true;
          });
        } else if (details.delta.dx < -1) {
          //Left Swipe
          if (isDrawerOpen == true) {
            setState(() {
              xOffset = 0;
              yOffset = 0;
              scaleFactor = 1;
              isDrawerOpen = false;
            });
          }
        }
      },
      child: AnimatedContainer(
          duration: Duration(milliseconds: 220),
          transform: Matrix4.translationValues(xOffset, yOffset, 0)
            ..scale(scaleFactor),
          decoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
            borderRadius: BorderRadius.circular(isDrawerOpen ? 40 : 0),
          ),
          child: Scaffold(
            body: SafeArea(
              child: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      backgroundColor: Colors.white38,
                      expandedHeight: 300.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        title: Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            'Sasurie College Of Engineering',
                            style: GoogleFonts.pacifico(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        background: Carousel(
                          boxFit: BoxFit.cover,
                          autoplay: true,
                          animationCurve: Curves.fastOutSlowIn,
                          animationDuration: Duration(milliseconds: 1000),
                          dotSize: 6.0,
                          dotIncreasedColor: Color(0xFFFF335C),
                          dotBgColor: Colors.transparent,
                          dotPosition: DotPosition.topRight,
                          dotVerticalPadding: 10.0,
                          showIndicator: false,
                          indicatorBgPadding: 7.0,
                          images: [
                            Opacity(
                              opacity: 0.80,
                              child: Image.asset(
                                "images/5.JPG",
                                fit: BoxFit.fill,
                              ),
                            ),
                            Opacity(
                              opacity: 0.80,
                              child: Image.asset(
                                "images/4.JPG",
                                fit: BoxFit.fill,
                              ),
                            ),
                            Opacity(
                              opacity: 0.50,
                              child: Image.asset(
                                "images/2.JPG",
                                fit: BoxFit.fill,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ];
                },
                body: Center(
                  child: ListView(
                    padding: EdgeInsets.symmetric(vertical: 30.0),
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 20.0, right: 120.0),
                        child: Text(
                          'Welcome to our College !',
                          style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 10.0),
                          child: Column(
                            children: [
                              Text(
                                'Sasurie College of Engineering is established by Ponmudi Muthusamy Gounder Trust, Tirupur in the year 2001 by its founder Chairman Sri A.M.Kandaswami a Philanthropist and Industrialist.',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal,
                                ),
                                textAlign: TextAlign.left,
                              ),
                              SizedBox(height: 9.0),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset("images/charmenimg.jpg"),
                              ),
                              Text(
                                "Sri A.M.Kandaswami",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.end,
                              )
                            ],
                          )),
                      SizedBox(height: 20.0),
                      InstitutionPage(),
                      SizedBox(height: 20.0),
                      CollegeFacilities(),
                      SizedBox(height: 20.0),
                      HotelCarousel(),
                      SizedBox(height: 20.0),
                      PlacementRecordsPage(),
                      SizedBox(height: 20.0),
                      GalleryImage(),
                      SizedBox(height: 20.0),
                      CoursesPage(),
                      SizedBox(height: 20.0),
                      LastPage(),
                    ],
                  ),
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton.extended(
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AdmissionPage()));
              },
              elevation: 20,
              label: Text('Admission Open'),
              icon: Icon(Icons.thumb_up),
              backgroundColor: Colors.pink,
            ),
          )),
    );
  }
}

class CollegeFacilities extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'College Toppers',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.5,
                ),
              ),
              // GestureDetector(
              //   onTap: () => print('See All'),
              //   child: Text(
              //     'See All',
              //     style: TextStyle(
              //       color: Theme.of(context).primaryColor,
              //       fontSize: 16.0,
              //       fontWeight: FontWeight.w600,
              //       letterSpacing: 1.0,
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
        Container(
          height: 300.0,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: destinations.length,
            itemBuilder: (BuildContext context, int index) {
              Destination destination = destinations[index];
              return GestureDetector(
                // onTap: () => Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (_) => DestinationScreen(
                //       destination: destination,
                //     ),
                //   ),
                // ),
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  width: 210.0,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: <Widget>[
                      Positioned(
                        bottom: 15.0,
                        child: Container(
                          height: 130.0,
                          width: 200.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  destination.city,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  destination.description,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w600,
                                    letterSpacing: 1.2,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.locationArrow,
                                      size: 10.0,
                                      color: Colors.black,
                                    ),
                                    SizedBox(width: 5.0),
                                    Text(
                                      destination.country,
                                      style: TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 6.0,
                            ),
                          ],
                        ),
                        child: Stack(
                          children: <Widget>[
                            Hero(
                              tag: destination.imageUrl,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: Image(
                                  height: 180.0,
                                  width: 180.0,
                                  image: NetworkImage(destination.imageUrl),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            // Positioned(
                            //   left: 10.0,
                            //   bottom: 10.0,
                            //   child: Column(
                            //     crossAxisAlignment: CrossAxisAlignment.start,
                            //     children: <Widget>[
                            //       //Seminar Hall
                            //       // Text(
                            //       //   destination.description,
                            //       //   style: TextStyle(
                            //       //     color: Colors.black,
                            //       //     fontSize: 10.0,
                            //       //     fontWeight: FontWeight.w600,
                            //       //     letterSpacing: 1.2,
                            //       //   ),
                            //       // ),
                            //       // Row(
                            //       //   children: <Widget>[
                            //       //     Icon(
                            //       //       FontAwesomeIcons.locationArrow,
                            //       //       size: 10.0,
                            //       //       color: Colors.black,
                            //       //     ),
                            //       //     SizedBox(width: 5.0),
                            //       //     Text(
                            //       //       destination.country,
                            //       //       style: TextStyle(
                            //       //         color: Colors.black,
                            //       //       ),
                            //       //     ),
                            //       //   ],
                            //       // ),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}

class HotelCarousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Facilities ',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.5,
                ),
              ),
              // GestureDetector(
              //   onTap: () => print('See All'),
              //   child: Text(
              //     'See All',
              //     style: TextStyle(
              //       color: Theme.of(context).primaryColor,
              //       fontSize: 16.0,
              //       fontWeight: FontWeight.w600,
              //       letterSpacing: 1.0,
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
        Container(
          height: 300.0,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: hotels.length,
            itemBuilder: (BuildContext context, int index) {
              Hotel hotel = hotels[index];
              return Container(
                margin: EdgeInsets.all(10.0),
                width: 240.0,
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Positioned(
                      bottom: 15.0,
                      child: Container(
                        height: 120.0,
                        width: 240.0,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                hotel.name,
                                style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 1.2,
                                ),
                              ),
                              SizedBox(height: 2.0),
                              // Text(
                              //   hotel.address,
                              //   style: TextStyle(
                              //     color: Colors.grey,
                              //   ),
                              // ),
                              // SizedBox(height: 2.0),
                              // Text(
                              //   '\$${hotel.price} / night',
                              //   style: TextStyle(
                              //     fontSize: 18.0,
                              //     fontWeight: FontWeight.w600,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 6.0,
                          ),
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: Image(
                          height: 180.0,
                          width: 220.0,
                          image: NetworkImage(hotel.imageUrl),
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}

class DestinationScreen extends StatefulWidget {
  final Destination destination;

  DestinationScreen({this.destination});

  @override
  _DestinationScreenState createState() => _DestinationScreenState();
}

class _DestinationScreenState extends State<DestinationScreen> {
  // Text _buildRatingStars(int rating) {
  //   String stars = '';
  //   for (int i = 0; i < rating; i++) {
  //     stars += '⭐ ';
  //   }
  //   stars.trim();
  //   return Text(stars);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0.0, 2.0),
                        blurRadius: 3.0,
                      ),
                    ],
                  ),
                  child: Hero(
                    tag: widget.destination.imageUrl,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30.0),
                      child: Image(
                        image: NetworkImage(widget.destination.imageUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.arrow_back),
                        iconSize: 30.0,
                        color: Colors.black,
                        onPressed: () => Navigator.pop(context),
                      ),
                      Row(
                        children: <Widget>[
                          // IconButton(
                          //   icon: Icon(Icons.search),
                          //   iconSize: 30.0,
                          //   color: Colors.black,
                          //   onPressed: () => Navigator.pop(context),
                          // ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.sortAmountDown),
                            iconSize: 25.0,
                            color: Colors.black,
                            onPressed: () => Navigator.pop(context),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 20.0,
                  bottom: 20.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.destination.city,
                        style: TextStyle(
                          color: Colors.yellow,
                          fontSize: 35.0,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 1.2,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            FontAwesomeIcons.locationArrow,
                            size: 15.0,
                            color: Colors.white70,
                          ),
                          SizedBox(width: 5.0),
                          Text(
                            widget.destination.country,
                            style: TextStyle(
                              color: Colors.white70,
                              fontSize: 20.0,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Positioned(
                  right: 20.0,
                  bottom: 20.0,
                  child: Icon(
                    Icons.location_on,
                    color: Colors.white70,
                    size: 25.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image(
                image: NetworkImage(widget.destination.imageUrl),
              ),
            ),
            SizedBox(height: 20),
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image(
                image: NetworkImage(widget.destination.imageUrl),
              ),
            ),
            SizedBox(height: 20),
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image(
                image: NetworkImage(widget.destination.imageUrl),
              ),
            )
          ],
        ),
      ],
    ));
  }
}

class Activity {
  String imageUrl;
  String name;
  String type;
  List<String> startTimes;
  int rating;
  int price;

  Activity({
    this.imageUrl,
    this.name,
    this.type,
    this.startTimes,
    this.rating,
    this.price,
  });
}

class Destination {
  String imageUrl;
  String city;
  String country;
  String description;
  List<Activity> activities;

  Destination({
    this.imageUrl,
    this.city,
    this.country,
    this.description,
    this.activities,
  });
}

List<Activity> activities = [
  Activity(
    imageUrl:
        'https://storage.googleapis.com/s3.codeapprun.io/userassets/pratik/BoZSEZyQbPstmarksbasilica.jpg',
    name: 'St. Mark\'s Basilica',
    type: 'Sightseeing Tour',
    startTimes: ['9:00 am', '11:00 am'],
    rating: 5,
    price: 30,
  ),
  Activity(
    imageUrl:
        'https://storage.googleapis.com/s3.codeapprun.io/userassets/pratik/VRpoLgzNCIgondola.jpg',
    name: 'Walking Tour and Gonadola Ride',
    type: 'Sightseeing Tour',
    startTimes: ['11:00 pm', '1:00 pm'],
    rating: 4,
    price: 210,
  ),
  Activity(
    imageUrl:
        'https://storage.googleapis.com/s3.codeapprun.io/userassets/pratik/cvXsNfIpPHmurano.jpg',
    name: 'Murano and Burano Tour',
    type: 'Sightseeing Tour',
    startTimes: ['12:30 pm', '2:00 pm'],
    rating: 3,
    price: 125,
  ),
];

List<Destination> destinations = [
  Destination(
    imageUrl: 'http://www.sasurieengg.com/topers/resize_1414141102.jpg',
    city: '7th RANK',
    country: 'IT',
    description: 'BAISHALIE KRISHNATRAYA',
    activities: activities,
  ),
  Destination(
    imageUrl: 'http://www.sasurieengg.com/topers/resize_1414139534.jpg',
    city: '8th RANK',
    country: 'IT',
    description: 'DHIRAVIYA MALAI JANANI S',
    activities: activities,
  ),
  Destination(
    imageUrl: 'http://www.sasurieengg.com/topers/resize_1414477266.jpg',
    city: '27th RANK',
    country: 'MBA',
    description: 'Sangeetha.T',
    activities: activities,
  ),
  Destination(
    imageUrl: 'http://www.sasurieengg.com/topers/resize_1568960319.jpg',
    city: '39th RANK',
    country: 'EEE',
    description: 'Balaabirami.B',
    activities: activities,
  ),
  // Destination(
  //   imageUrl: 'http://www.sasurieengg.com/gallery/ro-system.jpg',
  //   city: 'RO System ',
  //   country: 'SCE',
  //   description:
  //       'Purified  water provided in the campus and in the hostels to maintain a good hygiene.',
  //   activities: activities,
  // ),
  // Destination(
  //   imageUrl:
  //       'https://storage.googleapis.com/s3.codeapprun.io/userassets/pratik/hISkTptooSnewyork.jpg',
  //   city: 'New York City',
  //   country: 'United States',
  //   description: 'Visit New York for an amazing and unforgettable adventure.',
  //   activities: activities,
  // ),
];

class Hotel {
  String imageUrl;
  String name;
  String address;
  int price;

  Hotel({
    this.imageUrl,
    this.name,
    this.address,
    this.price,
  });
}

final List<Hotel> hotels = [
  Hotel(
    imageUrl:
        'https://www.amjaincollege.edu.in/images/os_imagegallery_205/original/dsc-0938.jpg',
    name: 'Seminar Halls',
    address: '404 Great St',
    price: 175,
  ),
  Hotel(
    imageUrl:
        'http://www.bahrauniversity.edu.in/wp-content/uploads/2019/03/hostel-2.jpg',
    name: 'Hostel',
    address: '404 Great St',
    price: 300,
  ),
  Hotel(
    imageUrl:
        'https://www.irishtimes.com/polopoly_fs/1.4199816.1583936929!/image/image.jpg_gen/derivatives/ratio_16x9_w1200/image.jpg',
    name: 'Cafeteria',
    address: '404 Great St',
    price: 240,
  ),
  Hotel(
    imageUrl: 'http://www.sasurieengg.com/gallery/transport/thumb/6.jpg',
    name: 'Transport',
    address: '404 Great St',
    price: 240,
  ),
  Hotel(
    imageUrl: 'http://www.sasurieengg.com/gallery/ro-system.jpg',
    name: 'RO System',
    address: '404 Great St',
    price: 240,
  ),
];
