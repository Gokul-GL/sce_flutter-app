import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PlacementRecordsPage extends StatefulWidget {
  const PlacementRecordsPage() : super();

  @override
  PlacementRecords createState() => PlacementRecords();
}

class PlacementRecords extends State<PlacementRecordsPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Placment Records',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.5,
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Container(
          height: 300.0,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: details.length,
            itemBuilder: (BuildContext context, int index) {
              Placementrecords placementrecords = details[index];
              return GestureDetector(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  width: 210.0,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: <Widget>[
                      Positioned(
                        bottom: 15.0,
                        child: Container(
                          height: 130.0,
                          width: 200.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  placementrecords.package,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  placementrecords.name,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w600,
                                    letterSpacing: 1.2,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.locationArrow,
                                      size: 10.0,
                                      color: Colors.black,
                                    ),
                                    SizedBox(width: 5.0),
                                    Text(
                                      placementrecords.companyname,
                                      style: TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 6.0,
                            ),
                          ],
                        ),
                        child: Stack(
                          children: <Widget>[
                            Hero(
                              tag: placementrecords.imageUrl,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: Image(
                                  height: 180.0,
                                  width: 180.0,
                                  image:
                                      NetworkImage(placementrecords.imageUrl),
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),

                            // Positioned(
                            //   left: 10.0,
                            //   bottom: 10.0,
                            //   child: Column(
                            //     crossAxisAlignment: CrossAxisAlignment.start,
                            //     children: <Widget>[
                            //       //Seminar Hall
                            //       // Text(
                            //       //   destination.description,
                            //       //   style: TextStyle(
                            //       //     color: Colors.black,
                            //       //     fontSize: 10.0,
                            //       //     fontWeight: FontWeight.w600,
                            //       //     letterSpacing: 1.2,
                            //       //   ),
                            //       // ),
                            //       // Row(
                            //       //   children: <Widget>[
                            //       //     Icon(
                            //       //       FontAwesomeIcons.locationArrow,
                            //       //       size: 10.0,
                            //       //       color: Colors.black,
                            //       //     ),
                            //       //     SizedBox(width: 5.0),
                            //       //     Text(
                            //       //       destination.country,
                            //       //       style: TextStyle(
                            //       //         color: Colors.black,
                            //       //       ),
                            //       //     ),
                            //       //   ],
                            //       // ),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}

class Placementrecords {
  String imageUrl;
  String name;
  String companyname;
  String package;

  Placementrecords({
    this.imageUrl,
    this.name,
    this.companyname,
    this.package,
  });
}

List<Placementrecords> details = [
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/IVTL%20Infoview/rajeshkumar.jpg",
    name: "RajeshKumar R",
    companyname: "IVTL INFOVIEW",
    package: "7-8 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/IVTL%20Infoview/Tendulkar.jpg",
    name: "Tendulkar P",
    companyname: "IVTL INFOVIEW",
    package: "7-8 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/Tavisca/ragubalagi.jpg",
    name: "Ragubalagi K",
    companyname: "TAVISCA",
    package: "6-7 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/Calibraint/murugalakshmi.jpg",
    name: "Murugalakshmi L",
    companyname: "CALIBRAINT",
    package: "4 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/IBIRDS/krishnakumar.jpg",
    name: "Krishnakumar K",
    companyname: "TCS",
    package: "4 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/Calibraint/mageshwari1.jpg",
    name: "Mageshwari L",
    companyname: "CALIBRAINT",
    package: "3.5 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/IBIRDS/krishnakumar.jpg",
    name: "Krishnakumar K",
    companyname: "CALIBRAINT",
    package: "4 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/NDOT/saranya.jpg",
    name: "Saranya R",
    companyname: "NDOT",
    package: "3.5 LPA",
  ),
  Placementrecords(
    imageUrl:
        "http://www.sasurieengg.com/placed-students-2018/Calibraint/murugalakshmi.jpg",
    name: "Murugalakshmi L",
    companyname: "TCS",
    package: "4 LPA",
  ),
];
