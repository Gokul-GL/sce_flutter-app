import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class GalleryImage extends StatefulWidget {
  const GalleryImage() : super();

  @override
  GalleryImageState createState() => GalleryImageState();
}

class GalleryImageState extends State<GalleryImage>
    with SingleTickerProviderStateMixin {
  // AnimationController _controller;
  // Animation<Offset> _offsetAnimation;

  // @override
  // void initState() {
  //   super.initState();
  //   _controller = AnimationController(
  //     duration: const Duration(seconds: 5),
  //     vsync: this,
  //   )..repeat(reverse: false);
  //   _offsetAnimation = Tween<Offset>(
  //     begin: Offset.zero,
  //     end: const Offset(1.5, 0.0),
  //   ).animate(CurvedAnimation(
  //     parent: _controller,
  //     curve: Curves.elasticIn,
  //   ));
  // }

  // @override
  // void dispose() {
  //   super.dispose();
  //   _controller.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Gallery',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.5,
                ),
              ),
              GestureDetector(
                onTap: () {
                  //Navigator.pop(context);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => GalleryView()));
                },
                child: Text(
                  'See All',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1.0,
                  ),
                ),
              ),
              
            ],
          ),
        ),
        // SlideTransition(
        //   position: _offsetAnimation,
        //   child:
        Padding(
          padding: EdgeInsets.all(10),
          child: Container(
            height: 300.0,
            child: GridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 05,
              mainAxisSpacing: 5,
              primary: false,
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 200,
                      height: 100,
                      child: Image.network(
                        "http://www.sasurieengg.com/admin-gallery/resize_1560493558.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Codienych#1.o",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 200,
                      height: 100,
                      child: Image.network(
                        "http://www.sasurieengg.com/news-events/resize_1581587423.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "15th Graduation Day",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      width: 200,
                      height: 100,
                      child: Image.network(
                        "http://www.sasurieengg.com/news-events/resize_1576468055.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "INFACT 2019",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      width: 200,
                      height: 100,
                      child: Image.network(
                        "http://www.sasurieengg.com/admin-gallery/resize_1560493558.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Project Expo 2019",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        )

        // ),
      ],
    );
  }
}

class GalleryView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Gallery",
            textAlign: TextAlign.center,
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 05,
            mainAxisSpacing: 5,
            primary: false,
            children: [
              Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/admin-gallery/resize_1560493558.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Codienych#1.o",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/news-events/resize_1581587423.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "15th Graduation Day",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/news-events/resize_1576468055.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "INFACT 2019",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/admin-gallery/resize_1560493558.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Project Expo 2019",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/admin-gallery/thumbnail_1519902120.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Tezfuerza 18 ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/admin-gallery/resize_1520463615.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Shinelogics Inaguration",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://www.sasurieengg.com/admin-gallery/resize_1519902447.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "SMILE 2K18",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      "http://knowafest.com/files/uploads/WhatsApp%20Image%202018-08-31%20at%209.49.16%20AM-2018083101.jpeg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Shahitya'18",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2,
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
