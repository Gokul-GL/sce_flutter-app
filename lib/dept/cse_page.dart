import 'package:SCE/gallery_image.dart';
import 'package:SCE/placement_records.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:url_launcher/url_launcher.dart';

class CSEPage extends StatefulWidget {
  const CSEPage() : super();

  @override
  CSEPageState createState() => CSEPageState();
}

class CSEPageState extends State<CSEPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: ListView(children: [
      Container(
          height: 300,
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: Color(0xff00cc76),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(70),
              ),
              boxShadow: [
                BoxShadow(
                  color: Color(0xff00aa95),
                  blurRadius: 10.0,
                ),
              ]),
          child: Stack(overflow: Overflow.visible, children: [
            Column(children: [
              Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          child: Icon(Icons.keyboard_return,
                              size: 30, color: Colors.white),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                        //Icon(Icons.favorite, size: 30, color: Colors.pink)
                      ])),
              SizedBox(height: 10),
              Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Department of Computer Science and Engineering",
                      style: GoogleFonts.roboto(
                          fontSize: 25,
                          color: Color(0xffffffff),
                          fontWeight: FontWeight.w600),
                    ),
                  )),
              SizedBox(height: 10),
              Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Heads of Department ",
                      style: GoogleFonts.roboto(
                          fontSize: 16,
                          color: Color(0xffffffff),
                          fontWeight: FontWeight.w500),
                    ),
                  )),
              SizedBox(height: 2),
              Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Mr.S.Prabakaran M.E (CSE)",
                      style: GoogleFonts.roboto(
                          fontSize: 15,
                          color: Color(0xffffffff),
                          fontWeight: FontWeight.w500),
                    ),
                  )),
              SizedBox(height: 10),
            ]),
            Positioned(
                bottom: -50,
                right: 0,
                child: Image.network(
                  "https://png2.cleanpng.com/sh/7b091e2515e310f3060d3eae4555ee19/L0KzQYm3VME2N6t1j5H0aYP2gLBuTfxieKV0iJ9sb33zhcXskr1paaNpj9N7ZT3xdcXAjCJscZ9sRdpqcnT6ccPsTfNwdaF6ReJsLUXkdISCWMUzPpdqe6cBLkC7QoiCWMQzOWY3S6o6NkW2QIW8WMUveJ9s/kisspng-laptop-computer-hardware-networking-hardware-compu-pc-5ad398526fec56.0827984215238165304585.png",
                  height: 200,
                  width: 300,
                ))
          ])),
      SizedBox(height: 50),
      Container(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Text("THE TECHNOCRATS OF THE SOCIETY",
            style: GoogleFonts.roboto(
                fontSize: 26,
                color: Color(0xff00aa95),
                fontWeight: FontWeight.w600)),
      ),
      SizedBox(height: 10),
      Container(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text("About the Department",
                  style: GoogleFonts.roboto(
                      fontSize: 20,
                      color: Color(0xff000000),
                      fontWeight: FontWeight.w600)),
            ),
            SizedBox(height: 4),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "   #  The department continues to grow at a rapid pace in terms of academic activities, publications, and national and international service and recognition.",
                style: GoogleFonts.roboto(
                    fontSize: 14,
                    color: Color(0xff000000),
                    fontWeight: FontWeight.w500),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "  #  Our rich academic environment directly contributes to the high quality of our undergraduate and graduate programs, supporting world-class education at all levels (B.E and M.E).",
                style: GoogleFonts.roboto(
                    fontSize: 14,
                    color: Color(0xff000000),
                    fontWeight: FontWeight.w500),
              ),
            ),
          ])),
      SizedBox(height: 10),
      Column(children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Lab Facilities',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                offset: Offset(0.0, 2.0),
                blurRadius: 6.0,
              ),
            ],
          ),
          child: SizedBox(
              height: 150.0,
              width: 350.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Carousel(
                  images: [
                    NetworkImage(
                        'http://www.sasurieengg.com/dept/cse/lab-2.JPG'),
                    NetworkImage(
                        'http://www.sasurieengg.com/dept/cse/lab-1.JPG'),
                    NetworkImage(
                        'http://www.sasurieengg.com/images/banner/cse/5.jpg'),
                    NetworkImage(
                        'http://www.sasurieengg.com/images/banner/cse/1.jpg'),
                  ],
                  boxFit: BoxFit.fill,
                  dotSize: 4.0,
                  dotSpacing: 15.0,
                  dotColor: Colors.lightGreenAccent,
                  indicatorBgPadding: 5.0,
                  //dotBgColor: Colors.purple.withOpacity(0.5),
                  borderRadius: true,
                  moveIndicatorFromBottom: 180.0,
                  noRadiusForIndicator: true,
                ),
              )),
        ),
      ]),
      SizedBox(height: 20),
      PlacementRecordsPage(),
      GalleryImage(),
      Align(
        alignment: Alignment.bottomCenter,
        child: Text(
          'For More Information Visite Our WebSite',
          style: TextStyle(
            fontSize: 10,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.5,
          ),
        ),
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: FlatButton(
            onPressed: () {
              launchURL();
            },
            child: Text("B.E. CSE")),
      ),
      SizedBox(height: 20),
    ])));
  }
}

launchURL() async {
  const url = 'http://www.sasurieengg.com/department-of-computer-science-and-engineering.php';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
